const babelOptions = {
  presets: [
    ['@babel/preset-react', { runtime: 'automatic' }],
    ['babel-preset-gatsby', { runtime: 'automatic' }],
    ['@babel/preset-typescript', { runtime: 'automatic' }],
  ],
};

module.exports = require('babel-jest').default.createTransformer(babelOptions);
