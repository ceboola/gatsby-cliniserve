const axios = require(`axios`);
const path = require(`path`);
const deburr = require('lodash.deburr');

exports.createPages = async ({ actions }) => {
  const { createPage } = actions;
  const districtsTemplate = path.resolve(
    `./src/templates/districts/DistrictsTemplate.tsx`,
  );
  const fetchedDistricts = () => {
    return axios
      .get('https://api.corona-zahlen.org/districts')
      .then((res) => res.data.data)
      .then((data) => data)
      .catch((err) => {
        console.log(err);
      });
  };
  const fetchedData = await fetchedDistricts();
  const districts = [].concat(...[fetchedData].map(Object.values));

  districts.map((district) => {
    return createPage({
      path: `/districts/${deburr(district.county)
        .replace(/\([^ ()]*\)/g, '')
        .trim()
        .replace(/\s+/g, '-')
        .toLocaleLowerCase()}`,
      component: districtsTemplate,
      context: { district },
    });
  });
};

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPlugin({
    name: '@babel/plugin-transform-react-jsx',
    options: {
      runtime: 'automatic',
    },
  });
};
