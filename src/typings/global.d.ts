interface DistrictsGlobal {
  ags: number;
  cases: number;
  casesPer100k: number;
  casesPerWeek: number;
  county: string;
  deaths: number;
  deathsPerWeek: number;
  delta: { cases: number; deaths: number; recovered: number };
  name: string;
  population: number;
  recovered: number;
  state: string;
  stateAbbreviation: string;
  weekIncidence: number;
}
