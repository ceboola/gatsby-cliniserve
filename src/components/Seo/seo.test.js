/**
 * @jest-environment jsdom
 */
import React from 'react';
import { render } from '@testing-library/react';
import { useStaticQuery } from 'gatsby';

import { Helmet } from 'react-helmet';
import SEO from '@components/Seo';

describe('SEO component', () => {
  beforeAll(() => {
    useStaticQuery.mockReturnValue({
      site: {
        siteMetadata: {
          title: `gatsby-cliniserve`,
          description: `A starter demonstrating what Gatsby can do.`,
          author: `ceboola`,
        },
      },
    });
  });

  it('renders the tests correctly', () => {
    const mockTitle = 'All posts | gatsby-cliniserve';
    const mockDescription = 'A starter demonstrating what Gatsby can do.';
    const mockTwitterHandler = 'ceboola';

    render(<SEO title="All posts" />);
    const { title, metaTags } = Helmet.peek();

    expect(title).toBe(mockTitle);
    expect(metaTags[0].content).toBe(mockDescription);
    expect(metaTags[5].content).toBe(mockTwitterHandler);
    expect(metaTags.length).toBe(8);
  });
});
