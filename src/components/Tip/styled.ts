import styled from 'styled-components';
import { Box } from 'grommet';

export const StyledBox = styled(Box)`
  position: absolute;
  top: 0;
  left: 0;
  padding: 0;
  height: auto;
`;
