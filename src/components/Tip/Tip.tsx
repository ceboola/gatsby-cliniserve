import { VFC } from 'react';
import { Box, Tip as GrommetTip, Text, Button } from 'grommet';
import { CircleInformation } from 'grommet-icons';

import { StyledBox } from './styled';
import { TipProps } from './types';

const Tip: VFC<TipProps> = ({ text }) => {
  return (
    <StyledBox fill gap="small" pad="small" align="start">
      <GrommetTip
        plain
        content={
          <Box
            background="background-front"
            pad="small"
            gap="small"
            width={{ max: 'small' }}
          >
            <Text weight="bold">Information</Text>
            <>
              <Text size="small">{text}</Text>
            </>
          </Box>
        }
        dropProps={{ align: { bottom: 'top' } }}
      >
        <Button icon={<CircleInformation size="medium" />} />
      </GrommetTip>
    </StyledBox>
  );
};

export default Tip;
