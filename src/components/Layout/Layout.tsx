import { FC, memo } from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { Grommet, Anchor, Box, Footer, Text } from 'grommet';
import { grommet } from 'grommet/themes';

import Header from '@components/Header';

const Layout: FC = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <Grommet
      theme={grommet}
      full
      style={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Header siteTitle={data.site.siteMetadata.title} />
      <Box as="main" pad="medium" flex overflow="auto">
        {children}
      </Box>
      <Footer background="dark-3" justify="center" pad="small">
        <Text textAlign="center" size="small">
          © {new Date().getFullYear()}, Built with
          {` `}
          <Anchor href="https://www.gatsbyjs.org">Gatsby</Anchor>
          {` and `}
          <Anchor href="https://v2.grommet.io">Grommet</Anchor>
        </Text>
      </Footer>
    </Grommet>
  );
};

export default memo(Layout);
