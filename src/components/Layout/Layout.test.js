/**
 * @jest-environment jsdom
 */
import React from 'react';
import { render, screen } from '@testing-library/react';

import Layout from './Layout';

import { useStaticQuery } from 'gatsby';

describe('Layout', () => {
  beforeAll(() => {
    useStaticQuery.mockReturnValue({
      site: {
        siteMetadata: {
          title: `gatsby-cliniserve`,
        },
      },
      allSitePage: {
        edges: [
          {
            node: {
              context: {
                district: {
                  county: 'Munchen',
                },
              },
            },
          },
        ],
      },
    });
  });

  test('renders Layout component with Header, Main, Footer', () => {
    render(<Layout />);
    expect(screen.getByRole('heading')).toHaveTextContent('gatsby-cliniserve');
    expect(screen.getByRole('main')).toBeVisible();
    expect(screen.getByRole('contentinfo')).toHaveTextContent(
      '© 2021, Built with Gatsby and Grommet',
    );
  });
  test('should render navigation button', () => {
    render(<Layout />);
    expect(screen.getByRole('button')).toHaveTextContent('districts');
    expect(screen.getByRole('button')).not.toBeDisabled();
    expect(screen.getByText('gatsby-cliniserve')).toHaveAttribute('href', '/');
  });
});
