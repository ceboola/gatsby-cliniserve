import { VFC } from 'react';
import { DataChart, Heading, Spinner } from 'grommet';
import dayjs from 'dayjs';

import { ChartProps } from './types';

const Chart: VFC<ChartProps> = ({ apiData, graphColor, title }) => {
  return apiData.length > 0 ? (
    <>
      <Heading level={3} size="small">
        {title}
      </Heading>
      <DataChart
        data={apiData}
        series={[
          {
            property: 'date',
            label: 'Date',
            render: (value) => `${dayjs(value).format('DD/MM/YYYY')}`,
          },
          { property: 'cases', label: 'cases' },
        ]}
        chart={[
          { property: 'cases', thickness: 'xsmall', type: 'area' },
          {
            property: 'cases',
            thickness: 'xsmall',
            type: 'point',
            point: 'circle',
            color: graphColor,
          },
        ]}
        guide={{ y: { granularity: 'fine' } }}
        size={{ width: 'fill' }}
        detail
        gap="small"
        legend={true}
      />
    </>
  ) : (
    <Spinner size="large" />
  );
};

export default Chart;
