import { ColorType } from 'grommet/utils';

export interface ChartProps {
  apiData: Array<{ cases: number; date: Date }>;
  graphColor: ColorType;
  title: string;
}
