export interface CityTableProps {
  district: string;
  state: string;
  population: number;
}
