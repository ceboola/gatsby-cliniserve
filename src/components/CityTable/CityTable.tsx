import { memo, VFC } from 'react';
import { DataTable } from 'grommet';

import { CityTableProps } from './types';

const CityTable: VFC<CityTableProps> = ({ district, state, population }) => {
  return (
    <>
      <DataTable
        columns={[
          {
            property: 'district',
            header: 'District',
            primary: true,
          },
          {
            property: 'state',
            header: 'State',
          },
          {
            property: 'population',
            header: 'Population',
          },
        ]}
        data={[
          {
            district,
            state,
            population,
          },
        ]}
      />
    </>
  );
};

export default memo(CityTable);
