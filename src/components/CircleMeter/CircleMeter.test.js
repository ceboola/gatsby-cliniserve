/**
 * @jest-environment jsdom
 */
import React from 'react';
import { render, screen } from '@testing-library/react';

import CircleMeter from './CircleMeter';

describe('CircleMeter', () => {
  test('should return value', () => {
    render(<CircleMeter meterValue={40} />);
    expect(screen.getByText(40)).toHaveTextContent(40);
  });
  test('should return gray circle color when no value', () => {
    const { container } = render(<CircleMeter meterValue={null} />);
    const circle = container.querySelector('circle');
    expect(circle).toHaveAttribute('stroke', '#DADADA');
  });
  test('should return green circle color when value', () => {
    const { container } = render(<CircleMeter meterValue={30} />);
    const path = container.querySelector('path');
    expect(path).toHaveAttribute('stroke', '#6FFFB0');
  });
});
