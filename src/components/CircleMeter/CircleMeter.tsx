import { Box, Stack, Meter, Text } from 'grommet';
import { VFC } from 'react';

import { StyledBox } from './styled';

const CircleMeter: VFC<{ meterValue: number }> = ({ meterValue }) => {
  return (
    <StyledBox align="center" pad="large" margin="0">
      <Stack anchor="center">
        <Meter
          data-testid="add"
          type="circle"
          background="light-4"
          values={[{ value: meterValue }]}
          size="xxsmall"
          thickness="xsmall"
        />
        <Box direction="row" align="center" pad={{ bottom: 'xsmall' }}>
          <Text size="xsmall" weight="bold">
            {meterValue}
          </Text>
          <Text size="xsmall">%</Text>
        </Box>
      </Stack>
    </StyledBox>
  );
};

export default CircleMeter;
