import styled from 'styled-components';
import { Box } from 'grommet';

export const StyledBox = styled(Box)`
  padding: 0;
`;
