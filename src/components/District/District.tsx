import { Heading } from 'grommet';
import { useEffect, useState, VFC } from 'react';
import axios from 'axios';

import County from '@components/County';

import { DistrictProps } from './types';

const District: VFC<DistrictProps> = ({ data }) => {
  const [fetchData, setFetchData] = useState([]);
  useEffect(() => {
    (async () => {
      try {
        const response = await axios.get(
          `https://api.corona-zahlen.org/districts/${data.district.ags}/history/cases/90`,
        );
        setFetchData(response.data.data);
      } catch (err) {
        setFetchData(err);
      }
    })();
  }, []);

  return (
    <>
      <Heading textAlign="center" level={2} size="small">
        {data.district.county}
      </Heading>
      <County apiData={fetchData[data.district.ags]} />
    </>
  );
};

export default District;
