import { VFC, memo } from 'react';
import { Header as GrommetHeader, Box, Menu, Heading } from 'grommet';
import { useStaticQuery, graphql, navigate } from 'gatsby';
import { AidOption } from 'grommet-icons';

import { sanitizeDiacriticsFromUrl } from '@src/helpers/sanitizeDiacriticsFromUrl';

import { StyledLink } from './styled';
import { HeaderProps, MapValue } from './types';

const Header: VFC<HeaderProps> = ({ siteTitle }) => {
  const allDistricts = useStaticQuery(graphql`
    query AllDistricts {
      allSitePage {
        edges {
          node {
            context {
              district {
                county
              }
            }
          }
        }
      }
    }
  `);

  const mappedDistricts = allDistricts.allSitePage.edges.map(
    (value: MapValue) => {
      const sanitizeName = sanitizeDiacriticsFromUrl(
        value.node.context?.district?.county,
      );

      return (
        value.node.context.district?.county !== undefined && {
          label: <Box pad="small">{value.node.context.district?.county}</Box>,
          onClick: () => navigate(`/districts/${sanitizeName}/`),
        }
      );
    },
  );

  return (
    <GrommetHeader background="dark-3" pad="medium" height="xsmall">
      <Heading size="small">
        <AidOption size="medium" color="#6fffb0" />
        <StyledLink to="/">{siteTitle}</StyledLink>
      </Heading>
      <Box justify="end">
        <Menu
          label="districts"
          a11yTitle="Navigation Menu"
          dropProps={{ align: { top: 'bottom', right: 'right' } }}
          items={mappedDistricts}
        />
      </Box>
    </GrommetHeader>
  );
};

export default memo(Header);
