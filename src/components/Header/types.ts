export interface HeaderProps {
  siteTitle: string;
}

export interface MapValue {
  node: {
    context: {
      district: {
        county: string;
      };
    };
  };
}
