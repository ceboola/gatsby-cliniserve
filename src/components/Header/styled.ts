import styled from 'styled-components';
import { Link } from 'gatsby';

export const StyledLink = styled(Link)`
  box-shadow: none;
  text-decoration: none;
  color: #6fffb0;
  margin-left: 10px;
`;
