import { VFC } from 'react';
import { Box, Tab, Tabs } from 'grommet';

import Chart from '@components/Chart';
import { CountyProps } from './types';

const County: VFC<CountyProps> = ({ apiData }) => {
  const apiData30Days = apiData?.history?.slice(60, 90) ?? [];
  const apiData90Days = apiData?.history ?? [];
  return (
    <Box margin="xsmall" width={{ min: 'small', max: 'large' }}>
      <Tabs flex>
        <Tab plain={false} title="90 days">
          <Chart
            apiData={apiData90Days}
            graphColor="graph-3"
            title="COVID-19 / Last 90 days"
          />
        </Tab>
        <Tab plain={false} title="30 days">
          <Chart
            apiData={apiData30Days}
            graphColor="graph-2"
            title="COVID-19 / Last 30 days"
          />
        </Tab>
      </Tabs>
    </Box>
  );
};

export default County;
