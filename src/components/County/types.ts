export interface CountyProps {
  apiData: {
    ags: string;
    history: [
      {
        cases: number;
        date: Date;
      },
    ];
    name: string;
  };
}
