import { useContext, memo, VFC } from 'react';
import { Box, Grid, ResponsiveContext } from 'grommet';

import CircleMeter from '@components/CircleMeter';
import Tip from '@components/Tip';
import { divideFn } from '@src/helpers/divideFn';

import { StyledHeading, StyledCard } from './styled';
import { CityProps } from './types';

const City: VFC<CityProps> = ({ data }) => {
  const size = useContext(ResponsiveContext);

  return (
    <Box pad="small">
      <Grid columns={size !== 'small' ? 'small' : '100%'} gap="small">
        <StyledCard pad="large">
          <Tip
            text={`Data in this column is divided by total cases: ${data.district.cases}`}
          />
          <StyledHeading margin="0" level={5} size="small">
            Total deaths
          </StyledHeading>
          <CircleMeter
            meterValue={
              divideFn(data.district.deaths, data.district.cases) * 100
            }
          />
          <Box align="center">{data.district.deaths}</Box>
        </StyledCard>

        <StyledCard pad="large">
          <Tip
            text={`Data in this column is divided by current population: ${data.district.population}`}
          />
          <StyledHeading margin="0" level={5} size="small">
            Total cases
          </StyledHeading>
          <CircleMeter
            meterValue={divideFn(data.district.cases, data.district.population)}
          />
          <Box align="center">{data.district.cases}</Box>
        </StyledCard>

        <StyledCard pad="large">
          <Tip
            text={`Data in this column is divided by total cases: ${data.district.cases}`}
          />
          <StyledHeading margin="0" level={5} size="small">
            Total recovered
          </StyledHeading>
          <CircleMeter
            meterValue={
              divideFn(data.district.recovered, data.district.cases) * 100
            }
          />
          <Box align="center">{data.district.recovered}</Box>
        </StyledCard>
      </Grid>
    </Box>
  );
};

export default memo(City);
