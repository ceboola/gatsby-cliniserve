import styled from 'styled-components';
import { Heading, Card } from 'grommet';

export const StyledHeading = styled(Heading)`
  display: flex;
  justify-content: center;
  font-size: 12px;
`;

export const StyledCard = styled(Card)`
  position: relative;
`;
