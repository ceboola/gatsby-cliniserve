import { useState, memo } from 'react';
import { WorldMap as GrommetWorldMap, Heading } from 'grommet';
import { navigate } from 'gatsby';

import { Wrapper, City, StyledBox, MapContainer } from './styled';

const WorldMap = () => {
  const [active, setActive] = useState(false);
  const [city, setCity] = useState('');
  const chooseCity = (city: string) => {
    setActive(!active);
    setCity(city);
  };
  const navigateToCity = () => navigate(`/districts/sk-${city.toLowerCase()}/`);

  const cities = [
    {
      name: 'Munchen',
      location: [48.137154, 11.576124],
      color: '#fd4a4a',
      onClick: () => navigateToCity(),
      onHover: () => {
        chooseCity('Munchen');
      },
    },
    {
      name: 'Dortmund',
      location: [51.514244, 7.468429],
      color: '#fd4a4a',
      onClick: () => navigateToCity(),
      onHover: () => {
        chooseCity('Dortmund');
      },
    },
  ];

  return (
    <Wrapper>
      <Heading margin="0">Choose city</Heading>
      <StyledBox align="center" pad="large">
        <City>{active && <Heading level={2}>{city}</Heading>}</City>
        <MapContainer>
          <GrommetWorldMap
            color="dark-3"
            places={cities}
            continents={[
              {
                name: 'Europe',
                color: 'graph-1',
              },
            ]}
          />
        </MapContainer>
      </StyledBox>
    </Wrapper>
  );
};

export default memo(WorldMap);
