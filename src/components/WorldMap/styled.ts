import styled from 'styled-components';
import { Box } from 'grommet';

export const Wrapper = styled.div`
  display: flex;
  width: auto;
  flex-direction: column;
`;

export const City = styled.div`
  height: 50px;
  margin: 16px 0;
`;

export const StyledBox = styled(Box)`
  padding-top: 0;
`;

export const MapContainer = styled.div`
  display: flex;
  justify-content: center;
`;
