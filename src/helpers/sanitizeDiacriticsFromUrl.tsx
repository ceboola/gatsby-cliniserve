import deburr from 'lodash.deburr';

export const sanitizeDiacriticsFromUrl = (text: string): string => {
  return deburr(text)
    .replace(/\([^ ()]*\)/g, '')
    .trim()
    .replace(/\s+/g, '-')
    .toLocaleLowerCase();
};
