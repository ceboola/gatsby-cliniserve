export const divideFn = (a: number, b: number): number => {
  return Number((a / b).toFixed(2));
};
