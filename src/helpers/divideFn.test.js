import { divideFn } from './divideFn';

describe('Divide function', () => {
  const a = 1234;
  const b = 31;
  const output = 39.81;

  const countDecimals = function (value) {
    if (value % 1 !== 0) return value.toString().split('.')[1].length;
    return 0;
  };
  it('should divide two numbers', () => {
    expect(divideFn(a, b)).toEqual(output);
  });
  it('should return typeof number', () => {
    expect(typeof divideFn(a, b)).toBe('number');
  });
  it('should return fixed-point notation to two decimals', () => {
    expect(countDecimals(divideFn(a, b))).toBe(2);
  });
});
