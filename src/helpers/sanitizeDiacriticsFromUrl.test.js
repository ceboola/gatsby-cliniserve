import { sanitizeDiacriticsFromUrl } from './sanitizeDiacriticsFromUrl';

describe('sanitizeDiacritics function', () => {
  const diacriticText = 'LK Spree-Neiße';
  const diacriticTextWithParenthesis = 'LK Rotenburg (Wümme)';

  it('should sanitize diacritics', () => {
    const output = 'lk-spree-neisse';
    expect(sanitizeDiacriticsFromUrl(diacriticText)).toEqual(output);
  });

  it('should return text without parenthesis', () => {
    const output = 'lk-rotenburg';
    expect(sanitizeDiacriticsFromUrl(diacriticTextWithParenthesis)).toEqual(
      output,
    );
  });
});
