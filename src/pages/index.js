import { memo } from 'react';
import Seo from '@components/Seo';
import WorldMap from '@components/WorldMap';

const IndexPage = () => {
  return (
    <>
      <Seo title="Home" />
      <WorldMap />
    </>
  );
};

export default memo(IndexPage);
