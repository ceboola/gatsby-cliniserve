import { VFC, memo } from 'react';
import { Grid, Box } from 'grommet';

import Seo from '@components/Seo';
import District from '@components/District';
import City from '@components/City';
import CityTable from '@components/CityTable';

import { DistrictsTemplateProps } from './types';

const DisctrictsTemplate: VFC<DistrictsTemplateProps> = ({ pageContext }) => {
  return (
    <>
      <Seo title={pageContext.district.name} />
      <Grid
        rows={['xsmall', 'auto']}
        columns={['60%', '40%']}
        areas={[
          ['info', 'info'],
          ['county', 'city'],
        ]}
        gap="small"
      >
        <Box background="light-3" gridArea="info">
          <CityTable
            district={pageContext.district.name}
            state={pageContext.district.state}
            population={pageContext.district.population}
          />
        </Box>
        <Box background="light-5" gridArea="county">
          <District data={pageContext} />
        </Box>

        <Box background="light-2" gridArea="city">
          <City data={pageContext} />
        </Box>
      </Grid>
    </>
  );
};

export default memo(DisctrictsTemplate);
