import Layout from '@components/Layout';

export const wrapPageElement = ({ element }) => {
  return <Layout>{element}</Layout>;
};
